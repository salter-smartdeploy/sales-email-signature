# EMAIL SIGNATURE

## Styles

A few link styles for this project are included in Salesperson-Style-Nurture becuase they had to go in the head:
```css
/* fix the link styles that gmail adds */
.footer-address a {
    color: #9f9f9f !important; 
    text-decoration: none;
}
.phone-number {
    color: #595655 !important; 
    text-decoration: none;
}
```

This one got a little messy, but the main files to worry about are:
* `dynamic-snippet.html` &ndash; This is the code that goes into the actual Marketo snippet  
* ~~`email-wrapper` &ndash; Any email that uses a signature snippet **has** to use these header styles.~~ I abandoned the mobile styles in email-wraper because it got too tedious to match the signature with the right email that had the remaining mobile code. Not the end of the world if gmail styles are missing. And it still scales fine on mobile.
